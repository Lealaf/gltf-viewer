#version 330

in vec3 vViewSpaceNormal;
in vec3 vViewSpacePosition;
in vec2 vTexCoords;

uniform vec3 uLightingDirection;
uniform vec3 uLightingIntensity;

uniform sampler2D uBaseColorTexture;
uniform vec4 uBaseColorFactor;

uniform float uMetallicFactor;
uniform float uRoughnessFactor;
uniform sampler2D uMetallicRoughnessTexture;

uniform sampler2D uEmissiveTexture;
uniform vec3 uEmissiveFactor;

uniform int uApplyOcclusion;
uniform sampler2D uOcclusionTexture;
uniform float uOcclusionStrength;

out vec3 fColor;


// Constants
const float GAMMA = 2.2;
const float INV_GAMMA = 1. / GAMMA;
const float M_PI = 3.141592653589793;
const float M_1_PI = 1.0 / M_PI;

// We need some simple tone mapping functions
// Basic gamma = 2.2 implementation
// Stolen here:
// https://github.com/KhronosGroup/glTF-Sample-Viewer/blob/master/src/shaders/tonemapping.glsl

// linear to sRGB approximation
// see http://chilliant.blogspot.com/2012/08/srgb-approximations-for-hlsl.html
vec3 LINEARtoSRGB(vec3 color) { return pow(color, vec3(INV_GAMMA)); }

// sRGB to linear approximation
// see http://chilliant.blogspot.com/2012/08/srgb-approximations-for-hlsl.html
vec4 SRGBtoLINEAR(vec4 srgbIn)
{
 return vec4(pow(srgbIn.xyz, vec3(GAMMA)), srgbIn.w);
}

float GeometrySchlickGGX(float NdotV, float roughness) {
  float r = (roughness + 1.0);
  float k = (r*r) / 8.0;

  float num   = NdotV;
  float denom = NdotV * (1.0 - k) + k;

  return num / denom;
}

float DistributionGGX(vec3 N, vec3 H, float roughness) {
  float a = roughness*roughness;
  float a2 = a * a;
  float NdotH  = clamp(dot(N, H), 0, 1);

  float denom = (NdotH*NdotH * (a2 - 1) + 1);

  return a2 / (M_1_PI * denom * denom);
}

float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness) {
  float NdotV = clamp(dot(N, V), 0, 1);
  float NdotL = clamp(dot(N, L), 0, 1);
  float ggx2  = GeometrySchlickGGX(NdotV, roughness);
  float ggx1  = GeometrySchlickGGX(NdotL, roughness);

  return ggx1 * ggx2;
}

void main()
{
 vec3 N = normalize(vViewSpaceNormal);
 vec3 L = uLightingDirection;
 vec3 V = normalize(-vViewSpacePosition);
 vec3 H = normalize(L + V);


 vec4 baseColorFromTexture =
 SRGBtoLINEAR(texture(uBaseColorTexture, vTexCoords));
 vec4 baseColor =baseColorFromTexture* uBaseColorFactor;


/*vec3 f_diffuse= diffuse_brdf(baseColor.rgb) ;
vec3 f_specular= ;*/


const vec3 black = vec3(0,0,0);
vec3  metallic= vec3(uMetallicFactor * texture(uMetallicRoughnessTexture, vTexCoords).b);
float roughness = uRoughnessFactor * texture(uMetallicRoughnessTexture, vTexCoords).g;
vec3 c_diff = mix(baseColor.rgb, black, metallic);
vec3 f0 = mix(vec3 (0.04), baseColor.rgb, metallic);
float alpha = roughness*roughness;
float VdotH =  clamp(dot(V, H), 0., 1.);
float VdotN =  clamp(dot(V, N), 0., 1.);
float LdotN =  clamp(dot(L, N), 0., 1.);
float NdotL = clamp(dot(N, L), 0., 1.);

float shlickFactor = (1 - abs(VdotH)) * (1 - abs(VdotH)); // power 2
shlickFactor *= shlickFactor; // power 4
shlickFactor *= (1 - abs(VdotH)); // power 5

vec3 F = f0 + (1 - f0) * shlickFactor;
float D = DistributionGGX(N, H, roughness);        
  float G = GeometrySmith(N, V, L, roughness); 

vec3 f_diffuse = (1 - F) * (1 / M_1_PI) * c_diff;
vec3 f_specular = F * D * G / (4 * abs(VdotN) * abs(LdotN));

vec3 emissive = SRGBtoLINEAR(texture2D(uEmissiveTexture, vTexCoords)).rgb *uEmissiveFactor;

vec3 material = f_diffuse + f_specular;
vec3 colorTmp= (f_diffuse + f_specular) * uLightingIntensity * NdotL + emissive;

if (1 == uApplyOcclusion) {
    float ao = texture2D(uOcclusionTexture, vTexCoords).r;
    colorTmp = mix(colorTmp, colorTmp * ao, uOcclusionStrength);
  }


fColor = LINEARtoSRGB(colorTmp);
}
