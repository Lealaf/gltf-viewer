#version 330

in vec3 vViewSpacePosition;
in vec3 vViewSpaceNormal;
in vec2 vTexCoords;

uniform vec3 uLightingDirection;
uniform vec3 uLightingIntensity;

out vec3 fColor;

void main()
{
   // Need another normalization because interpolation of vertex attributes does not maintain unit length
   vec3 viewSpaceNormal = normalize(vViewSpaceNormal);
   vec3 BRDF = vec3(1. / 3.14);
   fColor= BRDF* uLightingIntensity * dot(viewSpaceNormal, uLightingDirection);
}